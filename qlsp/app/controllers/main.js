const turnOnLoading = () => {
  document.querySelector(".loading-body").style.display = "flex";
};
const turnOffLoading = () => {
  document.querySelector(".loading-body").style.display = "none";
};
turnOnLoading();
const BASE_URL = "https://6271e18225fed8fcb5ec0cef.mockapi.io";
const renderDSSPServ = function () {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/san-pham`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res.data);
      exportDSSP(res.data);
    })
    .catch(function (e) {
      console.log("error", e);
    });
  turnOffLoading();
};
renderDSSPServ();

const deleteSpServ = function (id) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/san-pham/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      renderDSSPServ();
    })
    .catch(function (err) {
      console.log("error", err);
    });
};

const exportDSSP = function (arr) {
  var contentHTML = "";
  arr.forEach((e, i) => {
    var contentTr = `<tr>
      <td>${i + 1}</td>
      <td>${e.name}</td>
      <td>${e.price}</td>
      <td><img src="${e.imgUrl}" style="width: 200px;" /></td>
      <td>${e.type ? "Phone" : "Laptop"}</td>
      <td>
        <button class="btn btn-info" onclick="showEditSP(${e.id})">Sửa</button
        ><button class="btn btn-danger" onclick="deleteSpServ(${
          e.id
        })">Xoá</button>
      </td>
    </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
};

//then xử lý thành công
// catch xử lý thất bại

//lấy thông tin từ form
const getValueForm = () => {
  let id = document.getElementById("idSP").value * 1;
  let name = document.getElementById("TenSP").value;
  let price = document.getElementById("GiaSP").value;
  let imgUrl = document.getElementById("HinhSP").value;
  let type = document.getElementById("loaiSP").value * 1 == 0 ? true : false;

  return new SanPham(id, name, price, imgUrl, type);
};

//thêm mới sp
const addSP = () => {
  turnOnLoading();
  var newSP = getValueForm();

  axios({
    url: `${BASE_URL}/san-pham`,
    method: "POST",
    data: newSP,
  })
    .then((e) => {
      console.log(e);
      renderDSSPServ();
      $("#myModal").modal("hide");
    })
    .catch((err) => console.log(err));
  turnOffLoading();
};

const showEditSP = (id) => {
  document.getElementById("btnAdd").style.display = "none";
  document.getElementById("btnEdit").style.display = "block";

  axios({
    url: `${BASE_URL}/san-pham?id=${id}`,
    method: "GET",
  })
    .then((e) => {
      console.log(e.data);
      let sp = e.data[0];
      document.getElementById("idSP").value = sp.id;
      document.getElementById("TenSP").value = sp.name;
      document.getElementById("GiaSP").value = sp.price;
      document.getElementById("HinhSP").value = sp.imgUrl;
      document.getElementById("loaiSP").value = sp.type ? "0" : "1";
      $("#myModal").modal("show");
    })
    .catch((err) => console.log(err));
};

const editSp = () => {
  turnOnLoading();
  var editSP = getValueForm();

  axios({
    url: `${BASE_URL}/san-pham/${editSP.id}`,
    method: "PUT",
    data: editSP,
  })
    .then((e) => {
      console.log(e);
      renderDSSPServ();
      $("#myModal").modal("hide");
    })
    .catch((err) => console.log(err));
  turnOffLoading();
};

document.querySelector("#btnThemSP").addEventListener("click", () => {
  document.getElementById("btnAdd").style.display = "block";
  document.getElementById("btnEdit").style.display = "none";
  document.getElementById("form").reset();
});
