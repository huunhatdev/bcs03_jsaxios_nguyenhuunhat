const SanPham = function (_id, _name, _price, _imgUrl, _type) {
  this.id = _id;
  this.name = _name;
  this.price = _price;
  this.imgUrl = _imgUrl;
  this.type = _type;
};
